﻿using Microsoft.EntityFrameworkCore;
using RUVDS.TestWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RUVDS.TestWebApp.Data
{
    public class EfContext:DbContext
    {
        public EfContext(DbContextOptions<EfContext> options):base(options)
        {
            Database.EnsureCreated();
                }
        public DbSet<VirtualServer> VirtualServers { get; set; }
    }
}
