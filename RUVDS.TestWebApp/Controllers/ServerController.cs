﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RUVDS.TestWebApp.Data;
using RUVDS.TestWebApp.Models;

namespace RUVDS.TestWebApp.Controllers
{
    public class ServerController : Controller
    {
        private readonly EfContext context;
        public ServerController(IServiceProvider provider)
        {
            context = (EfContext)provider.GetService(typeof(EfContext));
        }
         

        public IActionResult Index()
        {
            var servers = context.VirtualServers.ToList();
            
            ViewBag.TotalTime = CalculateTotalUsageTime(servers);
            return View(servers);
        }

        public async Task<IActionResult> Add()
        {
            var newVirtualServer = new VirtualServer();

            context.VirtualServers.Add(newVirtualServer);
            await context.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Remove(string ids)
        {
            foreach(var serverId in ids.Split(','))
            {
                var serverToRemove = context.VirtualServers.FirstOrDefault(s => s.Id == int.Parse(serverId));
                serverToRemove.Remove();
            }
            
            await context.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        private TimeSpan CalculateTotalUsageTime(List<VirtualServer> servers)
        {
            long totalTicks = 0;
            var points = new KeyValuePair<long, bool>[servers.Count * 2];
            for (int i = 0; i < servers.Count; i++)
            {
                points[i * 2] = new KeyValuePair<long, bool>(servers[i].CreateDateTime.Ticks, false);
                points[i * 2 + 1] = new KeyValuePair<long, bool>(servers[i].RemoveDateTime.HasValue ? servers[i].RemoveDateTime.Value.Ticks : DateTime.Now.Ticks, true);
            }
            points.ToList().Sort((a, b) => (int)(b.Key - a.Key));
            int c = 0;
            for (int i = 0; i < points.Length; i++)
            {
                if (c != 0)
                    totalTicks += (points[i].Key - points[i - 1].Key);
                c = points[i].Value ? c - 1 : c + 1;
            }
            return new TimeSpan(totalTicks);
        }
    }
}
