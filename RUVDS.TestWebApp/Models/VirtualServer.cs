﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RUVDS.TestWebApp.Models
{
    public class VirtualServer
    {
        public int Id { get; private set; }
        public DateTime CreateDateTime { get; private set; }
        public DateTime? RemoveDateTime { get; private set; }
        public bool Removed { get; private set; }

        public VirtualServer()
        {
            CreateDateTime = DateTime.Now;
        }

        public void Remove()
        {
            if (!Removed)
            {
                Removed = true;
                RemoveDateTime = DateTime.Now;
            }
        }
    }
}
